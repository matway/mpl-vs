﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Text.Editor.OptionsExtensionMethods;
using Microsoft.VisualStudio.TextManager.Interop;
using MPL.AST;

namespace MPL.Commands {
  class CaretHandler : VSCommandTarget<VSConstants.VSStd2KCmdID> {
    private CaretPosition? caret;
    private ITextSnapshotLine nextTextLine;
    private ITextSnapshotLine currentTextLine;
    private ITextSnapshot currentSnap;
    private int textLineNum;
    private int offset;
    private int maxOffset;
    private int indentationSize;
    private TreeBuilder.Node lastToken;
    private Stack<Stack<TreeBuilder.Node>> bracketsStack;
    private Stack<TreeBuilder.Node> currentLevelBrackets;
    private bool wasPoped;
    private bool notReached;

    public CaretHandler(IVsTextView vsTextView, IWpfTextView textView) : base(vsTextView, textView) {
      maxOffset = 0;
    }

    protected override IEnumerable<VSConstants.VSStd2KCmdID> SupportedCommands {
      get {
        yield return VSConstants.VSStd2KCmdID.UP;
        yield return VSConstants.VSStd2KCmdID.DOWN;
        yield return VSConstants.VSStd2KCmdID.LEFT;
        yield return VSConstants.VSStd2KCmdID.RIGHT;
      }
    }

    protected override bool Execute(VSConstants.VSStd2KCmdID command, uint options, IntPtr pvaIn, IntPtr pvaOut) {
      if (MplPackage.completionSession) {
        return false;
      } else if (!TextView.Selection.IsEmpty) {
        return false;
      }

      caret = TextView.Caret.Position;
      if (caret == null) {
        return true;
      }

      offset = 0;
      notReached = true;
      currentSnap = TextView.TextSnapshot;
      lastToken = new TreeBuilder.Node { line = 0 };
      currentLevelBrackets = new Stack<TreeBuilder.Node>();
      bracketsStack = new Stack<Stack<TreeBuilder.Node>>();
      currentTextLine = currentSnap.GetLineFromPosition(caret.Value.BufferPosition.Position);
      textLineNum = currentSnap.GetLineNumberFromPosition(caret.Value.BufferPosition.Position);
      if (TextView.Options.IsConvertTabsToSpacesEnabled()) {
        indentationSize = TextView.Options.GetIndentSize();
      } else {
        indentationSize = 1;
      }

      switch (command) {
        case VSConstants.VSStd2KCmdID.UP:
        if (textLineNum == 0) {
          return true;
        }

        --textLineNum;
        break;
        case VSConstants.VSStd2KCmdID.DOWN:
        if (textLineNum == currentSnap.LineCount - 1) {
          return true;
        }

        ++textLineNum;
        break;
        case VSConstants.VSStd2KCmdID.LEFT:
        if (caret.Value.BufferPosition.Position != 0) {
          TextView.Caret.MoveToPreviousCaretPosition();
          maxOffset = Math.Abs(TextView.Caret.Position.BufferPosition.Difference(currentTextLine.Start)) + caret.Value.VirtualSpaces;
        }

        TextView.Caret.EnsureVisible();
        return true;
        case VSConstants.VSStd2KCmdID.RIGHT:
        if (caret.Value.BufferPosition.Position != TextView.TextSnapshot.Length) {
          TextView.Caret.MoveToNextCaretPosition();
          maxOffset = Math.Abs(TextView.Caret.Position.BufferPosition.Difference(currentTextLine.Start)) + caret.Value.VirtualSpaces;
        }

        TextView.Caret.EnsureVisible();
        return true;
      }

      nextTextLine = currentSnap.GetLineFromLineNumber(textLineNum);
      if (nextTextLine.Length == 0) {
        getOffset();
        TextView.Caret.MoveTo(new VirtualSnapshotPoint(nextTextLine, offset));
      } else {
        if (nextTextLine.Length < maxOffset) {
          TextView.Caret.MoveTo(nextTextLine.End);
        } else {
          TextView.Caret.MoveTo(new VirtualSnapshotPoint(nextTextLine, maxOffset));
        }
      }

      TextView.Caret.EnsureVisible();
      return true;
    }

    private void getOffset() {
      AST.AST.Parse(currentSnap.GetText(), out bool parsed);
      var root = AST.AST.GetASTRoot();
      if (root.name == "Program") {
        Traverse(root);
      }
    }

    private void Traverse(TreeBuilder.Node node) {
      if (node.children == null) {
        if (node.begin >= nextTextLine.Start.Position && notReached) {
          offset = bracketsStack.Count * indentationSize;
          notReached = false;
          return;
        }

        switch (node.name) {
          case "']'":
          case "'}'":
          case "')'":
          if (currentLevelBrackets.Count == 0 && bracketsStack.Count > 0 && bracketsStack.Peek().Count != 0) {
            if (node.line == bracketsStack.Peek().Peek().line) {
              bracketsStack.Peek().Pop();
              if (wasPoped) {
                currentLevelBrackets = bracketsStack.Pop();
                wasPoped = false;
              } else if (bracketsStack.Peek().Count == 0) {
                bracketsStack.Pop();
              }
            } else {
              currentLevelBrackets = bracketsStack.Pop();
              currentLevelBrackets.Pop();
            }
          } else if (currentLevelBrackets.Count > 0) {
            currentLevelBrackets.Pop();
          } else if (bracketsStack.Count > 0 && bracketsStack.Peek().Count == 0) {
            bracketsStack.Pop();
          }

          break;
          case "'['":
          case "'{'":
          case "'('":
          if (bracketsStack.Count == 0 && currentLevelBrackets.Count == 0) {
            bracketsStack.Push(new Stack<TreeBuilder.Node>());
            bracketsStack.Peek().Push(node);
          } else if (bracketsStack.Count != 0 && bracketsStack.Peek().Peek().line == node.line) {
            bracketsStack.Peek().Push(node);
          } else if (currentLevelBrackets.Count != 0) {
            bracketsStack.Push(new Stack<TreeBuilder.Node>(currentLevelBrackets));
            currentLevelBrackets.Clear();
            bracketsStack.Peek().Push(node);
            wasPoped = true;
          } else {
            bracketsStack.Push(new Stack<TreeBuilder.Node>());
            bracketsStack.Peek().Push(node);
          }

          break;
        }

        lastToken = node;
        return;
      }

      if (notReached) {
        foreach (var child in node.children) {
          Traverse(child);
        }
      }
    }

    protected override VSConstants.VSStd2KCmdID ConvertFromCommandId(uint id) {
      return (VSConstants.VSStd2KCmdID)id;
    }

    protected override uint ConvertFromCommand(VSConstants.VSStd2KCmdID command) {
      return (uint)command;
    }
  }
}
